<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use FOS\UserBundle\Model\User as BaseUser;

// *** AQUÍ PODERÍASE CAMBIAR O NOME DA TÁBOA fos_user POR usuario POR EXEMPLO
// *** PARA ENGADIR CAMPOS NON DEBERÍA HABER PROBLEMA..
// *** PARA ELIMINAR O CAMPO 'username' => DEFINIR AQUÍ TODA A CLASE USUARIO?

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

//*************************** CAMPOS EXTRA DE USUARIOS: Teléfono, URL, Whatsapp, FB, Twitter, Centro, etc...

/**
 * @ORM\Column(type="string", length=20, nullable=true)
 */
protected $nome;

/**
 * @ORM\Column(type="string", length=100, nullable=true)
 */
protected $apelidos;

/**
 * @ORM\Column(type="string", length=100, nullable=true)
 */
protected $telefono;

/**
 * @ORM\Column(type="string", length=255, nullable=true)
 * @Assert\Url()
 */
protected $web;

// UN USUARIO PODE INDICAR O SEU CENTRO EDUCATIVO.
//************************************************
//* @ORM\ManyToOne(targetEntity="School", inversedBy="id")
//* @ORM\JoinColumn(name="id", referencedColumnName="id")

/**
* @ORM\ManyToOne(targetEntity="School")
 */
protected $school;

/**
 * @ORM\Column(type="datetime", nullable=true)
 */
protected $datahoraInscricion;








    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Set web
     *
     * @param string $web
     * @return User
     */
    public function setWeb($web)
    {
        $this->web = $web;

        return $this;
    }

    /**
     * Get web
     *
     * @return string
     */
    public function getWeb()
    {
        return $this->web;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return User
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set datahoraInscricion
     *
     * @param \DateTime $datahoraInscricion
     * @return User
     */
    public function setDatahoraInscricion($datahoraInscricion)
    {
        $this->datahoraInscricion = $datahoraInscricion;

        return $this;
    }

    /**
     * Get datahoraInscricion
     *
     * @return \DateTime
     */
    public function getDatahoraInscricion()
    {
        return $this->datahoraInscricion;
    }

    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return User
     */
    public function setSchool(\AppBundle\Entity\School $school = null)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return User
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set apelidos
     *
     * @param string $apelidos
     * @return User
     */
    public function setApelidos($apelidos)
    {
        $this->apelidos = $apelidos;

        return $this;
    }

    /**
     * Get apelidos
     *
     * @return string
     */
    public function getApelidos()
    {
        return $this->apelidos;
    }
}
