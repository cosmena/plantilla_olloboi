<?php
// src/AppBundle/Entity/Video.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\VideoRepository")
 * @ORM\Table(name="video")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"url"},
 *     message="A Url introducida xa foi inscrita no concurso!"
 * )
  * @Assert\GroupSequence({"UrlVimeoYoutube", "Video"})
 */
class Video
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $url;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Choice(callback = "getEstados")
     */
    protected $estado;

    public static function getEstados()
    {
        return array("INSCRITO", "ACEPTADO", "PENDENTE REVISIÓN", "REXEITADO");
    }



    /**
     * @ORM\Column(type="text")
     */
    protected $credits;

    /**
     * @ORM\Column(type="string", length=255)
     */
    // can be youtube or vimeo
    protected $proveedor;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="username_id", referencedColumnName="id")
     */

    // UN VÍDEO TEN ASOCIADO O USUARIO QUE O ENVIOU
    //*********************************************
    protected $username;

    // UN VÍDEO TEN ASOCIADO UN NIVEL EDUCATIVO AO QUE PERTENCE O ALUMNADO QUE PARTICIPOU NEL
    //***************************************************************************************
//    * @ORM\ManyToOne(targetEntity="EducationLevel", inversedBy="name")

    /**
     * @ORM\ManyToOne(targetEntity="EducationLevel")
     * @ORM\JoinColumn(name="educationlevel_id", referencedColumnName="id")
     */
    // one video has an education level
    protected $educationLevel;

    // UN VÍDEO TEN ASOCIADO UN CENTRO EDUCATIVO DE REFERENCIA
    //********************************************************
    //     * @ORM\ManyToOne(targetEntity="School", inversedBy="school")
    //     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
    /**
     * @ORM\ManyToOne(targetEntity="School")
     * @ORM\JoinColumn(name="school_id", referencedColumnName="id")
     */
    // Un vídeo está asociado a un centro de referencia.
    protected $school;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    // opcional
    protected $responsableCentro;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    // opcional
    protected $responsableEmailCentro;

    /**
     * @ORM\Column(type="boolean")
     */
    // marcar obligatoriamente
//    protected $aceptacionBases;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    protected $comments;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $datahoraInscricion;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }



    /**
     * Set title
     *
     * @param string $title
     *
     *
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }



    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return Video
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }



    /**
      * @Assert\IsTrue(message = "A Url introducida non parece de Vimeo ou Youtube.", groups = {"UrlVimeoYoutube"})
      */
    public function isYoutubeOrVimeo()
    {
      // Examina a url e comproba se é de Youtube ou de Vimeo.
      //      En Youtube pode fallar nas listas
      //      Garda o Proveedor e acorta a URL ou devolve NULL
      //      !!! PROBAR MÁIS CONCIENZUDAMENTE. REVISAR E MELLORAR
      // é YOUTUBE?
      if ($recorte = strpos($this->url, "list")) {
        $this->url = substr($this->url, 0, $recorte);
      }
      $regexstr = '~
      # Match Youtube link and embed code
      (?:				 # Group to match embed codes
         (?:<iframe [^>]*src=")?	 # If iframe match up to first quote of src
         |(?:				 # Group to match if older embed
            (?:<object .*>)?		 # Match opening Object tag
            (?:<param .*</param>)*     # Match all param tags
            (?:<embed [^>]*src=")?     # Match embed tag to the first quote of src
         )?				 # End older embed code group
      )?				 # End embed code groups
      (?:				 # Group youtube url
         https?:\/\/		         # Either http or https
         (?:[\w]+\.)*		         # Optional subdomains
         (?:               	         # Group host alternatives.
             youtu\.be/      	         # Either youtu.be,
             | youtube\.com		 # or youtube.com
             | youtube-nocookie\.com	 # or youtube-nocookie.com
         )				 # End Host Group
         (?:\S*[^\w\-\s])?       	 # Extra stuff up to VIDEO_ID
         ([\w\-]{11})		         # $1: VIDEO_ID is numeric
         [^\s]*			 # Not a space
      )				 # End group
      "?				 # Match end quote if part of src
      (?:[^>]*>)?			 # Match any extra stuff up to close brace
      (?:				 # Group to match last embed code
         </iframe>		         # Match the end of the iframe
         |</embed></object>	         # or Match the end of the older embed
      )?				 # End Group of last bit of embed code
      ~ix';

      $iframestr = '$1';

      $Resultado=preg_replace($regexstr, $iframestr, $this->url);
      // preg_replace: si se cumplen las reglas devuelve el código del vídeo
      //                sino devuelve la url de entrada
      if ($Resultado != $this->url) {
         $this->url = $Resultado;
         $this->proveedor = "YOUTUBE";

         /*$validator = $this->get('validator');
         $errors = $validator->validate($this);
         if (count($errors) == 0) {*/
            return true;
        /*}*/
      }




      // ou é VIMEO?
      $regexstr = '~
       # Match Vimeo link and embed code
      (?:<iframe [^>]*src=")? 	# If iframe match up to first quote of src
      (?:				# Group vimeo url
      	https?:\/\/		# Either http or https
      	(?:[\w]+\.)*		# Optional subdomains
      	vimeo\.com		# Match vimeo.com
      	(?:[\/\w]*\/videos?)?	# Optional video sub directory this handles groups links also
      	\/			# Slash before Id
      	([0-9]+)		# $1: VIDEO_ID is numeric
      	[^\s]*			# Not a space
      )				# End group
      "?				# Match end quote if part of src
      (?:[^>]*></iframe>)?		# Match the end of the iframe
      (?:<p>.*</p>)?		        # Match any title information stuff
      ~ix';

      $iframestr = '$1';

      $Resultado=preg_replace($regexstr, $iframestr, $this->url);
      // preg_replace: si se cumplen las reglas devuelve el código del vídeo
      //                sino devuelve la url de entrada

      if ($Resultado != $this->url) {
         $this->url = $Resultado;
         $this->proveedor = "VIMEO";
         return true;
      }

    // SI A URL NON É DE VIMEO OU YOUTUBE

      return false;

        //return $this->url;
    }


    /**
     * Set credits
     *
     * @param string $credits
     *
     * @return Product
     */
    public function setCredits($credits)
    {
        $this->credits = $credits;

        return $this;
    }

    /**
     * Get credits
     *
     * @return string
     */
    public function getCredits()
    {
        return $this->credits;
    }

    /**
    * Set educationLevel
    *
    * @param \AppBundle\Entity\EducationLevel $educationLevel
    *
    * @return Video
    */
//    public function setEducationLevel(\AppBundle\Entity\EducationLevel $educationLevel)
    public function setEducationLevel($educationLevel)
   {
       $this->educationLevel = $educationLevel;

       return $this;
   }

   /**
    * Get educationLevel
    *
    * @return \AppBundle\Entity\EducationLevel
    */
   public function getEducationLevel()
   {
       return $this->educationLevel;
   }

    /**
     * Set responsableCentro
     *
     * @param string $responsableCentro
     * @return Video
     */
    public function setResponsableCentro($responsableCentro)
    {
        $this->responsableCentro = $responsableCentro;

        return $this;
    }

    /**
     * Get responsableCentro
     *
     * @return string
     */
    public function getResponsableCentro()
    {
        return $this->responsableCentro;
    }

    /**
     * Set responsableEmailCentro
     *
     * @param string $responsableEmailCentro
     * @return Video
     */
    public function setResponsableEmailCentro($responsableEmailCentro)
    {
        $this->responsableEmailCentro = $responsableEmailCentro;

        return $this;
    }

    /**
     * Get responsableEmailCentro
     *
     * @return string
     */
    public function getResponsableEmailCentro()
    {
        return $this->responsableEmailCentro;
    }

/*    /**
     * Set aceptacionBases
     *
     * @param boolean $aceptacionBases
     * @return Video
     */
/*    public function setAceptacionBases($aceptacionBases)
    {
        $this->aceptacionBases = $aceptacionBases;

        return $this;
    }

    /**
     * Get aceptacionBases
     *
     * @return boolean
     */
/*    public function getAceptacionBases()
    {
        return $this->aceptacionBases;
    }
*/

    /**
     * Set comments
     *
     * @param string $comments
     * @return Video
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set datahoraInscricion
     *
     * @param \DateTime $datahoraInscricion
     * @return Video
     */
    public function setDatahoraInscricion($datahoraInscricion)
    {
        $this->datahoraInscricion = $datahoraInscricion;

        return $this;
    }

    /**
     * Get datahoraInscricion
     *
     * @return \DateTime
     */
    public function getDatahoraInscricion()
    {
        return $this->datahoraInscricion;
    }


    /**
     * Set proveedor
     *
     * @param string $proveedor
     * @return Video
     */
    public function setProveedor($proveedor)
    {
        $this->proveedor = $proveedor;

        return $this;
    }

    /**
     * Get proveedor
     *
     * @return string
     */
    public function getProveedor()
    {
        return $this->proveedor;
    }


    /**
     * Set school
     *
     * @param \AppBundle\Entity\School $school
     * @return Video
     */
//     public function setSchool(\AppBundle\Entity\School $school = null)
     public function setSchool($school)
    {
        $this->school = $school;

        return $this;
    }

    /**
     * Get school
     *
     * @return \AppBundle\Entity\School
     */
    public function getSchool()
    {
        return $this->school;
    }


    public function enviarEmailVideoInscrito($video)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Olloboi 2016. Novo Vídeo Inscrito! :)')
            ->setFrom('info@olloboi.com')
            ->setTo('cosmena@gmail.com')    // ******* Correo electrónico del Usuario Identificado. username_email
            ->setBody(                          // ******* Enviar tamén a info@olloboi.com e a responsableEmailCentro
                $this->renderView(
                    // app/Resources/views/Emails/registration.html.twig
                    'email/video_inscrito.html.twig',
                    array('name' => $name)      // ******* Vídeo inscrito: Datos del vídeo $video-
                ),
                'text/html'
            )
            /*
             * If you also want to include a plaintext version of the message
            ->addPart(
                $this->renderView(
                    'email/video_inscrito.txt.twig',
                    array('name' => $name)
                ),
                'text/plain'
            )*/
        ;
        $this->get('mailer')->send($message);

        // return $this->render(...);
    }






    /**
     * Set estado
     *
     * @param string $estado
     * @return Video
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }

    /**
     * Get estado
     *
     * @return string
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * Set username
     *
     * @param \AppBundle\Entity\User $username
     * @return Video
     */
    public function setUsername(\AppBundle\Entity\User $username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return \AppBundle\Entity\User
     */
    public function getUsername()
    {
        return $this->username;
    }
}
