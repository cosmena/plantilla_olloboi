<?php
// src/AppBundle/Entity/Categoria.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\CategoriaRepository")
 * @ORM\Table(name="categoria")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity(
 *     fields={"nome"},
 *     message="O nome de categoría debe ser único!"
 * )
 */

class Categoria
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

     /**
      * @ORM\Column(type="text")
      */
    protected $descricion;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $tipo;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return Categoria
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set descricion
     *
     * @param string $descricion
     * @return Categoria
     */
    public function setDescricion($descricion)
    {
        $this->descricion = $descricion;

        return $this;
    }

    /**
     * Get descricion
     *
     * @return string
     */
    public function getDescricion()
    {
        return $this->descricion;
    }

    /**
     * Set tipo
     *
     * @param string $tipo
     * @return Categoria
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * Get tipo
     *
     * @return string
     */
    public function getTipo()
    {
        return $this->tipo;
    }
}
