<?php

// src/AppBundle/Entity/School.php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\SchoolRepository")
 * @ORM\Table(name="school")
 * @ORM\HasLifecycleCallbacks()
 */
class School
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\OneToMany(targetEntity="User", mappedBy="school")
     */
    protected $id;



    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $nome;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email
     */
    protected $email;

    /**
     * @ORM\Column(type="text")
     */
    protected $enderezo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $concello;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $provincia;

    /**
     * @ORM\Column(type="string", length=5)
     */
    protected $cp;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $telefono;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $coordx;

    /**
     * @ORM\Column(type="string", length=20)
     */
    protected $coordy;

    /**
     * @ORM\Column(type="string", length=10)
     */
    protected $titularidade;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $concertado;

    /**
     * @ORM\Column(type="string", length=3)
     */
    protected $dependente;


    /**
     * Set id
     *
     * @param integer $id
     * @return School
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nome
     *
     * @param string $nome
     * @return School
     */
    public function setNome($nome)
    {
        $this->nome = $nome;

        return $this;
    }

    /**
     * Get nome
     *
     * @return string
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return School
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set url
     *
     * @param string $url
     * @return School
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set enderezo
     *
     * @param string $enderezo
     * @return School
     */
    public function setEnderezo($enderezo)
    {
        $this->enderezo = $enderezo;

        return $this;
    }

    /**
     * Get enderezo
     *
     * @return string
     */
    public function getEnderezo()
    {
        return $this->enderezo;
    }

    /**
     * Set concello
     *
     * @param string $concello
     * @return School
     */
    public function setConcello($concello)
    {
        $this->concello = $concello;

        return $this;
    }

    /**
     * Get concello
     *
     * @return string
     */
    public function getConcello()
    {
        return $this->concello;
    }

    /**
     * Set provincia
     *
     * @param string $provincia
     * @return School
     */
    public function setProvincia($provincia)
    {
        $this->provincia = $provincia;

        return $this;
    }

    /**
     * Get provincia
     *
     * @return string
     */
    public function getProvincia()
    {
        return $this->provincia;
    }

    /**
     * Set cp
     *
     * @param string $cp
     * @return School
     */
    public function setCp($cp)
    {
        $this->cp = $cp;

        return $this;
    }

    /**
     * Get cp
     *
     * @return string
     */
    public function getCp()
    {
        return $this->cp;
    }

    /**
     * Set telefono
     *
     * @param string $telefono
     * @return School
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;

        return $this;
    }

    /**
     * Get telefono
     *
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return School
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    public function __toString()
    {
        $Cadena=$this->getNome()." - ".$this->getConcello();
        return $Cadena;
    }


    /**
     * Set coordx
     *
     * @param string $coordx
     * @return School
     */
    public function setCoordx($coordx)
    {
        $this->coordx = $coordx;

        return $this;
    }

    /**
     * Get coordx
     *
     * @return string
     */
    public function getCoordx()
    {
        return $this->coordx;
    }

    /**
     * Set coordy
     *
     * @param string $coordy
     * @return School
     */
    public function setCoordy($coordy)
    {
        $this->coordy = $coordy;

        return $this;
    }

    /**
     * Get coordy
     *
     * @return string
     */
    public function getCoordy()
    {
        return $this->coordy;
    }

    /**
     * Set titularidade
     *
     * @param string $titularidade
     * @return School
     */
    public function setTitularidade($titularidade)
    {
        $this->titularidade = $titularidade;

        return $this;
    }

    /**
     * Get titularidade
     *
     * @return string
     */
    public function getTitularidade()
    {
        return $this->titularidade;
    }

    /**
     * Set concertado
     *
     * @param string $concertado
     * @return School
     */
    public function setConcertado($concertado)
    {
        $this->concertado = $concertado;

        return $this;
    }

    /**
     * Get concertado
     *
     * @return string
     */
    public function getConcertado()
    {
        return $this->concertado;
    }

    /**
     * Set dependente
     *
     * @param string $dependente
     * @return School
     */
    public function setDependente($dependente)
    {
        $this->dependente = $dependente;

        return $this;
    }

    /**
     * Get dependente
     *
     * @return string
     */
    public function getDependente()
    {
        return $this->dependente;
    }

}
