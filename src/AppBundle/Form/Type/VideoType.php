<?php
namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use AppBundle\Entity\User;
use AppBundle\Controller\VideoController;

class VideoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // *** PENDENTE: Presentar o centro do usuario como o centro por defecto.
        //$user = $user->getUser();
        $builder
            ->add('url', UrlType::class, array('label'  => 'Url *',))
            ->add('title', TextType::class, array('label'  => 'Título *',))
            ->add('credits', TextareaType::class, array('label'  => 'Descrición, Sinopse e Créditos *',))
            ->add('educationLevel', 'entity', array('label'  => 'Nivel Educativo *',
                                                    'class' => 'AppBundle:EducationLevel',))
            ->add('school', 'entity', array('label'  => 'Centro Educativo *',
                                                        'class' => 'AppBundle:School',
                                                        'query_builder' => function (EntityRepository $er) {
                                                                  return $er->createQueryBuilder('u')
                                                                  ->orderBy('u.nome', 'ASC');
                                                                  },
        //                                                'preferred_choices' => array($user->getSchool())
                                                            ))
            ->add('responsableCentro', TextType::class, array('label'  => 'Responsable do Centro Educativo',
                                                              'required' => false,))
            ->add('responsableEmailCentro', EmailType::class, array('label'  => 'Correo-e Responsable do Centro Educativo',
                                                              'required' => false,))
            ->add('comments', TextareaType::class, array('label'  => 'Outros comentarios (privados para a organización)',
                                                              'required' => false,))
            ->add('save', SubmitType::class, array('label' => 'Inscribir Vídeo'))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(array(
	        'data_class' => 'AppBundle\Entity\Video',
	    ));
	}

}
