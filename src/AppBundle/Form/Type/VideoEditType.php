<?php
namespace AppBundle\Form\Type;

use Doctrine\ORM\EntityRepository;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

use AppBundle\Controller\VideoController;

use AppBundle\Entity\User;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Model\UserInterface;

class VideoEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $em = $this->getDoctrine()->getEntityManager();
        $builder
            ->add('title', TextType::class, array('label'  => 'Título *',))
            ->add('credits', TextareaType::class, array('label'  => 'Descrición, Sinopse e Créditos *',))
            ->add('educationLevel', 'entity', array('label'  => 'Nivel Educativo *',
                                                    'class' => 'AppBundle:EducationLevel',))
            ->add('school', 'entity', array('label'  => 'Centro Educativo *',
                                                        'class' => 'AppBundle:School',
                                                        'query_builder' => function (EntityRepository $er) {
                                                                  return $er->createQueryBuilder('u')
                                                                  ->orderBy('u.nome', 'ASC');
                                                                  },
        //                                                'data'=>$em->getReference("AppBundle:School")
                                                            ))
            ->add('responsableCentro', TextType::class, array('label'  => 'Responsable do Centro Educativo',
                                                              'required' => false,))
            ->add('responsableEmailCentro', EmailType::class, array('label'  => 'Correo-e Responsable do Centro Educativo',
                                                              'required' => false,))
            ->add('comments', TextareaType::class, array('label'  => 'Outros comentarios (privados para a organización)',
                                                              'required' => false,))
            ->add('estado', ChoiceType::class, array('label'  => 'Estado do vídeo',
                                                              'required' => false,
                                                              'choices'  => array(
                                                                       'INSCRITO',
                                                                       'ACEPTADO',
                                                                       'PENDENTE REVISIÓN',
                                                                       'REXEITADO',
                                                                   ),
  //                                                            'data' => $estado,
            ))
            ->add('save', SubmitType::class, array('label' => 'Editar Vídeo',))
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
	{
	    $resolver->setDefaults(array(
	        'data_class' => 'AppBundle\Entity\Video',
	    ));
	}

}
