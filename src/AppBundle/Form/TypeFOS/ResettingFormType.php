<?php

namespace AppBundle\Form\TypeFOS;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ResettingFormType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder->add('plainPassword', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\RepeatedType'), array(
          'type' => LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'),
          'options' => array('translation_domain' => 'FOSUserBundle'),
          'first_options' => array('label' => 'Novo Contrasinal'),
          'second_options' => array('label' => 'Repetir Novo Contrasinal'),
          'invalid_message' => 'Os contrasinais non coinciden :O',
      ));
  }

  public function getParent()
  {
      return 'FOS\UserBundle\Form\Type\ResettingFormType';
  }

    public function getBlockPrefix()
    {
        return 'app_user_resetting';
    }
}
