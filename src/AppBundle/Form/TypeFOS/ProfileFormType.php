<?php

namespace AppBundle\Form\TypeFOS;

use Doctrine\ORM\EntityRepository;

use FOS\UserBundle\Util\LegacyFormHelper;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class ProfileFormType extends AbstractType
{

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $this->buildUserForm($builder, $options);

      $builder->add('nome', TextType::class, array('label'  => 'Nome','required' => false,))
              ->add('apelidos', TextType::class, array('label'  => 'Apelidos','required' => false,))
              ->add('telefono', TextType::class, array('label'  => 'Teléfono','required' => false,))
              ->add('web', UrlType::class, array('label'  => 'Web','required' => false,))
              ->add('school', 'entity', array('label'  => 'Centro Educativo *',
                                                          'class' => 'AppBundle:School',
                                                          'query_builder' => function (EntityRepository $er) {
                                                                    return $er->createQueryBuilder('u')
                                                                    ->orderBy('u.nome', 'ASC');
                                                                    },))
              ->add('current_password', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\PasswordType'), array(
                            'label' => 'Contrasinal Actual',
                            'translation_domain' => 'FOSUserBundle',
                            'mapped' => false,
                            'constraints' => new UserPassword(),))

          ;
  }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\ProfileFormType';
    }

    public function getBlockPrefix()
    {
        return 'app_user_profile';
    }

    /**
     * Builds the embedded form representing the user.
     *
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    protected function buildUserForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', null, array('label' => 'Usuari@', 'translation_domain' => 'FOSUserBundle'))
            ->add('email', LegacyFormHelper::getType('Symfony\Component\Form\Extension\Core\Type\EmailType'), array('label' => 'Correo-e', 'translation_domain' => 'FOSUserBundle'))
        ;
    }
}
