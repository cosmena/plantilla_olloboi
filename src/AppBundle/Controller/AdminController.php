<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Form\Type\VideoType;

class AdminController extends Controller
{
  /**
   * @Route("/admin")
   */
   public function adminAction(Request $request)
   {
     $user = $this->getUser();

     // Obtén os vídeos
     $repository = $this->getDoctrine()
               ->getRepository('AppBundle:Video');
     $videos = $repository->findAll();
           //    ->findByEstado('INSCRITO');
          //     ->findByEstado("ACEPTADO");

     // Imprime a plantilla
     return $this->render('admin.html.twig');
   }

     /**
      * @Route("/admin/videos")
      */
      public function adminVideosAction(Request $request)
      {
        $user = $this->getUser();

        // Obtén os vídeos
        $repository = $this->getDoctrine()
                  ->getRepository('AppBundle:Video');
        $videos = $repository->findAll();
              //    ->findByEstado('INSCRITO');
             //     ->findByEstado("ACEPTADO");

        // Imprime a plantilla
        return $this->render('adminVideos.html.twig', array(
                                'user' => $user,
                                'videos' => $videos
          ));
      }

   /**
    * @Route("/admin/users")
    */
    public function adminUsersAction(Request $request)
    {
      $user = $this->getUser();

      // Obtén os usuarios
      $userManager = $this->get('fos_user.user_manager');
      $usuarios = $userManager->findUsers();

      // Imprime a plantilla
      return $this->render('adminUsers.html.twig', array(
                              'user' => $user,
                              'usuarios' => $usuarios
        ));
    }

}
