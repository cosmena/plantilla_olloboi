<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Form\Type\VideoType;
use AppBundle\Form\Type\VideoEditType;

class VideoController extends Controller
{
    /**
     * @Route("/video/new/")
     */
##################  FASE DE INSCRICIÓN
/*    public function newAction(Request $request)
    {
        // Crea o obxecto Video
        $video = new Video();
        // Inicializa propiedades
        //$video->setUrl('Pega aquí a dirección do vídeo');

        // Crea o formulario
        $form = $this->createForm(VideoType::class, $video);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // ... perform some action, such as saving the task to the database

            // set datahoraInscricion
            $date = date_create();
            $video->setDatahoraInscricion ($date);
            $video->setEstado ('INSCRITO');
            $user = $this->getUser();
            $video->setUsername ($user);
            // persist database
            $em = $this->getDoctrine()->getManager();
            $em->persist($video);
            $em->flush();

            // COMPOÑER E ENVIAR EMAIL A: USUARIO, PROFE RESPONSABLE E ORGANIZACIÓN
            //*********************************************************************
            $user = $this->getUser();

            $message = \Swift_Message::newInstance()
                ->setSubject('Novo Vídeo Inscrito: '.$video->getTitle())
                ->setFrom(array('info@olloboi.com' => 'Festival Audiovisual Escolar Olloboi'))
                ->setTo($user->getEmail())    // ******* Correo electrónico del Usuario Identificado.
                ->setBcc('info@olloboi.com')  // ** Copia á Organización ******************
                ->setBody(
                    $this->renderView(
                        'email/video_inscrito.html.twig',
                        array('user' => $user, 'video' => $video)      // ******* Vídeo inscrito: Datos del vídeo $video-
                    ),
                    'text/html'
                );

            if ($video->getResponsableEmailCentro()) {
                $message->setCc(array($video->getResponsableEmailCentro() => $video->getResponsableCentro()));
              }
*/
                /*
                 * If you also want to include a plaintext version of the message
                ->addPart(
                    $this->renderView(
                        'email/video_inscrito.txt.twig',
                        array('name' => $name)
                    ),
                    'text/plain'
                )*/
/*            ;
            $this->get('mailer')->send($message);
            // REDIRECCIONAR A VIDEO INSCRITO? OU RENDEriZAR A PLANTILLA DIRECTAMENTE?
            // return $this->redirectToRoute('video_inscrito');
            return $this->render('video/inscrito.html.twig', array("video" => $video));
        }

        return $this->render('video/new.html.twig', array('form' => $form->createView(), ));
    }
*/

    // *********************** AMOSA UN VIDEO COA ID DADA
    /**
     * @Route("/video/show/{videoId}")
     */
    public function showVideo($videoId)
    {
      // Busca o vídeo pola id
    	$video = $this->getDoctrine()
                ->getRepository('AppBundle:Video')
                ->find($videoId);

      if (!$video) {
          throw $this->createNotFoundException(
              'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
          );};

          if ($video->getEstado()=="INSCRITO") {
              $user = $this->getUser();
              if (!$user || !$user->hasRole('ROLE_ADMIN'))
                  throw $this->createNotFoundException(
                    'Sentímolo, pero non tes permiso para acceder ao vídeo con ese identificador: '.$videoId
              );};



        return $this->render('video/show.html.twig', array("video" => $video, ));
      }





      // *********************** EDITA UN VIDEO COA ID DADA
      /**
       * @Route("/video/edit/{videoId}")
       */
      public function editVideo($videoId, Request $request)
      {
        // Obtén o usuario
        $user = $this->getUser();
        // Só os administradores poden editar vídeos
        if (!$user || !$user->hasRole('ROLE_ADMIN'))
            throw $this->createNotFoundException(
              'Sentímolo, pero non tes permiso para acceder ao vídeo con ese identificador: '.$videoId);

        // Busca o vídeo pola id
      	$video = $this->getDoctrine()
                  ->getRepository('AppBundle:Video')
                  ->find($videoId);

        // Se non existe o vídeo.. malo
        if (!$video) {
            throw $this->createNotFoundException(
                'Sentímolo, pero non existe un vídeo con ese identificador: '.$videoId
            );};

        // Busca o autor do vídeo
        $userVideo = $this->getDoctrine()
                ->getRepository('AppBundle:User')
                ->find($video->getUsername());

        // Presenta o formulario de edición
        $form = $this->createForm(VideoEditType::class, $video);

        $form->handleRequest($request);
/*        if ($video->getProveedor()=="YOUTUBE") {
            $url="https://youtu.be/".$video->geturl();
            $video->setUrl($url);
          }

        elseif ($video->getProveedor()=="VIMEO") {
            $url="https://vimeo.com/".$video->geturl();
            $video->setUrl($url);
        }
*/

        if ($form->isSubmitted()) {
//          if ($form->isValid()) {
            // ... perform some action, such as saving the task to the database
            // persist database
            $em = $this->getDoctrine()->getManager();
            $em->persist($video);
            $em->flush();

            /* COMPOÑER E ENVIAR EMAIL A: USUARIO, PROFE RESPONSABLE E ORGANIZACIÓN
            /**********************************************************************/

            if ($video->getEstado()=="ACEPTADO") {
              return $this->render(
                  'email/video_aceptado.html.twig',
                  array('user' => $userVideo, 'video' => $video));
            };
            if ($video->getEstado()=="INSCRITO") {
              return $this->render(
                  'email/video_aceptado.html.twig',
                  array('user' => $userVideo, 'video' => $video));
            };
//        }
      }

        return $this->render('video/edit.html.twig', array('form' => $form->createView(), 'video' => $video ));
        }

}
