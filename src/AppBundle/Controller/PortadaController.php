<?php
// ELIMINAR ESTE CONTIDO DE EXEMPLO????
//*****************************************

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class PortadaController extends Controller
{
  /**
   * @Route("/portada", name="portada")
   */
  public function portadaAction(Request $request)
  {
      // Renderiza y devuelve la plantilla
      return $this->render('portada.html.twig'
  //, array('base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),)
       );


  }
  /**
   * @Route("/elements", name="elements")
   */
  public function elementsAction(Request $request)
  {
      // Renderiza y devuelve la plantilla
      return $this->render('elements.html'
  //, array('base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),)
       );


  }
}
