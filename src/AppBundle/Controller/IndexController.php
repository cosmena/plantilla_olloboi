<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class IndexController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction(Request $request)
    {
        return $this->render('index.html.twig'
		//, array('base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),)
	       );
    }


    /**
     * @Route("/creadores")
     */
    public function creadoresAction(Request $request)
    {
        return $this->render('creadores.html.twig');
    }

    /**
     * @Route("/complices")
     */
    public function complicesAction(Request $request)
    {
        return $this->render('complices.html.twig');
    }

    /**
     * @Route("/encontro")
     */
    public function encontroAction(Request $request)
    {
        return $this->render('encontro.html.twig');
    }

    /**
     * @Route("/videos")
     */
    public function videosAction(Request $request)
    {

      $user = $this->getUser();

      // Obtén os vídeos
      $repository = $this->getDoctrine()->getRepository('AppBundle:Video');

      $videos = $repository->findByEstado("ACEPTADO");

      // Imprime a plantilla co usuario e os vídeos
      return $this->render('videos.html.twig', array(
                              'user' => $user,
                              'videos' => $videos
        ));


    }

/**
 * @Route("/premios")
 */
public function premiosAction(Request $request)
{

  $user = $this->getUser();

// OBTER OS VIDEOS GAÑADORES
  $em = $this->getDoctrine()->getManager();
  $query = $em->createQuery(
      'SELECT s
      FROM AppBundle:Seleccionado s
      JOIN s.video v
      WHERE s.estado = :estado
      ORDER BY s.categoria ASC'
  )       ->setParameter('estado', 'PREMIO');
  $videosPremios = $query->getResult();

  // Imprime a plantilla co usuario e os vídeos
  return $this->render('premios.html.twig', array(
                          'user' => $user,
                          'videosPremios' => $videosPremios
    ));


}


    /**
     * @Route("/finalistas")
     */
    public function finalistasAction(Request $request)
    {

      $user = $this->getUser();

/*      // Obtén os vídeos
      $repository = $this->getDoctrine()->getRepository('AppBundle:Seleccionado')
                  ->findBy(
                        array('categoria' => '1')
//                        array('Estado' => array("FINALISTA", "CANDIDATO"))
                          );
      //->findByCategoria('1');

      $videos = $this->getDoctrine()->getRepository('AppBundle:Video')
                      //->findAll()
                      ->findByEstado(array("INSCRITO", "ACEPTADO"));

      //$repository->findByEstado("FINALISTA");
*/


// OBTENER CATEGORIAS
// Falta esta parte...


// PARA CADA CATEGORIA OBTENER LOS VIDEOS FINALISTAS+CANDIDATOS
      $em = $this->getDoctrine()->getManager();

      // SELECCIONADOS Categoría CÓMICO (1)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
            AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '1')
       ->setParameter('estado1', 'FINALISTA')
       ->setParameter('estado2', 'CANDIDATO');
      $videosComico = $query->getResult();

      // SELECCIONADOS Categoría PELEÓN (2)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '2')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');
      $videosPeleon = $query->getResult();

      // SELECCIONADOS Categoría FOTOXÉNICO (3)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '3')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosFotoxenico = $query->getResult();

      // SELECCIONADOS Categoría DJ (4)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '4')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosDJ = $query->getResult();

      // SELECCIONADOS Categoría GUIÓN (5)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '5')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosGuion = $query->getResult();

      // SELECCIONADOS Categoría ANIMADO (6)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '6')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosAnimado = $query->getResult();

      // SELECCIONADOS Categoría DOC (7)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '7')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosDoc = $query->getResult();

      // SELECCIONADOS Categoría UNIVERSITARIO (8)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '8')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosUniversitario = $query->getResult();

      // SELECCIONADOS Categoría PÚBLICO (9)
      $query = $em->createQuery(
          'SELECT s
          FROM AppBundle:Seleccionado s
          JOIN s.video v
          WHERE s.categoria = :categoria
          AND (s.estado = :estado1 OR s.estado = :estado2)
          ORDER BY v.id ASC'
      )->setParameter('categoria', '9')
      ->setParameter('estado1', 'FINALISTA')
      ->setParameter('estado2', 'CANDIDATO');

      $videosPublico = $query->getResult();



      // Imprime a plantilla co usuario e os vídeos
      return $this->render('finalistas.html.twig', array(
                              'user' => $user,
                              'videosComico' => $videosComico,
                              'videosPeleon' => $videosPeleon,
                              'videosFotoxenico' => $videosFotoxenico,
                              'videosDJ' => $videosDJ,
                              'videosGuion' => $videosGuion,
                              'videosAnimado' => $videosAnimado,
                              'videosDoc' => $videosDoc,
                              'videosUniversitario' => $videosUniversitario,
                              'videosPublico' => $videosPublico
        ));


    }



    /**
     * @Route("/sobre-olloboi")
     */
    public function sobreAction(Request $request)
    {
        return $this->render('sobre-olloboi.html.twig');
    }


    /**
     * @Route("/prensa-e-medios")
     */
    public function prensaAction(Request $request)
    {
        return $this->render('prensa-e-medios.html.twig');
    }


    /**
     * @Route("/bases")
     */
    public function basesAction(Request $request)
    {
        return $this->render('bases.html.twig');
    }

    /**
     * @Route("/faq")
     */
    public function faqAction(Request $request)
    {

      return $this->render('faq.html.twig');
    }


}
