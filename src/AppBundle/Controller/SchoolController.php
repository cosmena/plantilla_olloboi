<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use AppBundle\Entity\User;
use AppBundle\Entity\Video;
use AppBundle\Entity\School;

class SchoolController extends Controller
{

    // *********************** AMOSA UN CENTRO EDUCATIVO COA ID DADA
    /**
     * @Route("/school/show/{schoolId}")
     */
    public function showSchool($schoolId)
    {
      // Busca a escola pola id
    	$school = $this->getDoctrine()
                ->getRepository('AppBundle:School')
                ->find($schoolId);

      if (!$school) {
          throw $this->createNotFoundException(
              'Sentímolo, pero non existe un centro educativo con ese identificador: '.$schoolId
          );};

      // Obtén os vídeos
      $repository = $this->getDoctrine()->getRepository('AppBundle:Video');

      $videos = $repository->findBy(array('estado'=>"ACEPTADO", 'school'=>$school));


      return $this->render('school/show.html.twig', array("school" => $school, "videos" => $videos));
      }
}
